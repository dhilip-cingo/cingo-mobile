/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('Cingo.services').factory('HttpService', ['$http', '$q', HttpService])

function HttpService($http, $q) {
    var baseUrl = "https://peaceful-plateau-39923.herokuapp.com/api/v1/";
    var authTokenForSession;
    var globalCustomerId;
    return {
        setAuthToken:setAuthToken,
        signupNewUser: signupNewUser,
        signIn: signIn,
        updateUserInfo: updateUserInfo,
        getVendorsList:getVendorsList,
        createVendorForUser:createVendorForUser,
        createUserRequest:createUserRequest,
        setCustomerId:setCustomerId
    };
    
    function setAuthToken(auth_token){
        authTokenForSession = auth_token;
    }
    
    function setCustomerId(customerId){
        globalCustomerId = customerId;
    }

    function signupNewUser(user, callback) {
        var signupUrl = baseUrl + 'customers';
        var customer = {"customer": {
                "email": user.email,
                "password": user.password,
                "password_confirmation": user.password,
                "name": user.email,
                "mobile_number": "514.364.1309"
            }};
        var customerAsJSON = JSON.stringify(customer);

        console.log(customerAsJSON);
        $http.post(signupUrl, customerAsJSON)
                .success(function (data) {
                    console.log('Success signup WS ' + JSON.stringify(data));
                    callback({"customer_id": data.customer_id});
                })
                .error(function (data) {
                    if (data === null) {
                        callback({"error": "Error in Signup"});
                        return;
                    }
                    console.log('Error in signup WS ' + JSON.stringify(data));
                    if (data.success === "false") {
                        callback({"error": "Error in Signup"});
                    }

                });


    }
    ;

    function signIn(user, callback) {
        var signupUrl = baseUrl + 'customers/sign_in';
        var customer = {"customer": {
                "email": user.email,
                "password": user.password,
            }};

        var customerAsJSON = JSON.stringify(customer);
        console.log(customerAsJSON);
        $http.post(signupUrl, customerAsJSON)
                .success(function (data) {
                    console.log('Success signIn WS ' + JSON.stringify(data));
                    callback({"token": data.customer.token, "auth_token": data.customer.auth_token, "session": data.customer.session,"customer_id":data.customer.id});
                })
                .error(function (data) {
                    if (data && data.success === "false") {
                        console.log('Error in signIn WS ' + JSON.stringify(data));
                        callback({"error": "Error in signIn"});
                    }

                });
    }
    ;
function createVendorForUser(vendorAccount,userId, callback) {
        var auth_token = authTokenForSession;
        var userRequestUrl = baseUrl + 'customers/' + userId + '/vendor_accounts';
        var requestAsJson = JSON.stringify(vendorAccount);
        console.log('Creating vendor '+requestAsJson);
        $http.post(userRequestUrl, requestAsJson, {
            headers: {'Authorization': auth_token}})
                .success(function (data) {
                    console.log('Success vendor request WS ' + JSON.stringify(data));
                    callback({});
                })
                .error(function (data) {
                    if (data && data.success === "false") {
                        console.log('Error in user request WS ' + JSON.stringify(data));
                        callback({"error": "Error in request creation"});
                    }

                });
    }
    ;
    function updateUserInfo(user, auth_token, callback) {
        var signupUrl = baseUrl + 'customers/' + user.customer_id;
        var customer = {"customer": {
                "email": user.email,
                "password": user.password,
                "password_confirmation": user.password,
                "name": user.email,
                "mobile_number": "514.364.1309"
            }};

        var customerAsJSON = JSON.stringify(customer);
        console.log(customerAsJSON);
        $http.put(signupUrl, customerAsJSON, {
            headers: {'Authorization': auth_token}})
                .success(function (data) {
                    console.log('Success settings WS ' + JSON.stringify(data));
                    callback({"customer_id": data.customer_id});
                })
                .error(function (data) {
                    console.log('Error in settings WS ' + JSON.stringify(data));
                    if (data.success === "false") {
                        callback({"error": "Error in Signup"});
                    }

                });


    }
    ;
    
//     function addNewRequest(request,callback,auth_token) {
//        var signupUrl = baseUrl + 'customers/'+globalCustomerId+'/user_requests';
//       var customer = {"customer": {
//                "customer_id": globalCustomerId,
//                "request_time": request.requestTime,
//                "description": request.requestDescription,
//                "department_id": request.department,
//                "vendor_id": request.vendorId
//            }};
//       
//        var customerAsJSON = JSON.stringify(customer);
//        console.log(customerAsJSON);
//        $http.put(signupUrl, customerAsJSON,{
//    headers: {'Authorization':auth_token}})
//                .success(function (data) {
//                    console.log('Success settings WS ' + JSON.stringify(data));
//                    callback({"customer_id": data.customer_id});
//                })
//                .error(function (data) {
//                    console.log('Error in settings WS ' + JSON.stringify(data));
//                    if (data.success === "false") {
//                        callback({"error": "Error in Signup"});
//                    }
//
//                })};
    function createUserRequest(request, callback) {
        request.customer_id = globalCustomerId;
        var userRequestUrl = baseUrl + 'customers/' + globalCustomerId + '/user_requests';
        var requestObject = {"user_request": request};
        var requestAsJson = JSON.stringify(requestObject);
        $http.post(userRequestUrl, requestAsJson, {
            headers: {'Authorization': authTokenForSession}})
                .success(function (data) {
                    console.log('Success User request WS ' + JSON.stringify(data));
                    callback('');
                })
                .error(function (data) {
                    if (data && data.success === "false") {
                        console.log('Error in user request WS ' + JSON.stringify(data));
                        callback({"error": "Error in request creation"});
                    }

                });
    }
    ;

    function modifyUserRequest(request, callback, auth_token) {
        var userRequestUrl = baseurl + 'customers/' + request.userId + '/user_requests' / request.user_request_id;
        var requestObject = {"user_request": request};
        var requestAsJson = JSON.stringify(requestObject);
        $http.post(userRequestUrl, requestAsJson, {
            headers: {'Authorization': auth_token}})
                .success(function (data) {
                    console.log('Success modify User request WS ' + JSON.stringify(data));
                    callback('');
                })
                .error(function (data) {
                    if (data && data.success === "false") {
                        console.log('Error in modify request WS ' + JSON.stringify(data));
                        callback({"error": "Error in request creation"});
                    }

                });
    }
    ;

    function postCustomerRating(request, rating, callback, auth_token) {
        var userRequestUrl = baseurl + 'customers/' + request.userId + '/user_requests' / request.user_request_id / 'user_requests_rating';
        var ratingObject = {"rating": rating};
        var ratingAsJson = JSON.stringify(ratingObject);
        $http.post(userRequestUrl, ratingAsJson, {
            headers: {'Authorization': auth_token}})
                .success(function (data) {
                    console.log('Success post rating WS ' + JSON.stringify(data));
                    callback('');
                })
                .error(function (data) {
                    if (data && data.success === "false") {
                        console.log('Error in post rating WS ' + JSON.stringify(data));
                        callback({"error": "Error in request creation"});
                    }

                });
    }
    ;

    function addRequestNotes(request, note, callback, auth_token) {
        var userRequestUrl = baseurl + 'customers/' + request.userId + '/user_requests' / request.user_request_id / 'user_requests_notes';
        var noteObject = {"note": note};
        var noteObjectAsJson = JSON.stringify(noteObject);
        $http.post(userRequestUrl, noteObjectAsJson, {
            headers: {'Authorization': auth_token}})
                .success(function (data) {
                    console.log('Success add User request notes ' + JSON.stringify(data));
                    callback('');
                })
                .error(function (data) {
                    if (data && data.success === "false") {
                        console.log('Error add user request notes ' + JSON.stringify(data));
                        callback({"error": "Error in request creation"});
                    }

                });
    }
    ;
    
    
 

    function modifyVendorAccountForUser(userId, vendorAccount,callback,auth_token) {
        var userRequestUrl = baseurl + 'customers/' + userId + '/vendor_accounts' /vendorAccount.vendor_id;
        var requestObject = {"accountNumber": vendorAccount.account_number};
        var requestAsJson = JSON.stringify(requestObject);
        $http.put(userRequestUrl, requestAsJson, {
            headers: {'Authorization': auth_token}})
                .success(function (data) {
                    console.log('Success modify User request WS ' + JSON.stringify(data));
                    callback({"token": data.customer.token, "auth_token": data.customer.auth_token, "session": data.customer.session});
                })
                .error(function (data) {
                    if (data && data.success === "false") {
                        console.log('Error in modify request WS ' + JSON.stringify(data));
                        callback({"error": "Error in request creation"});
                    }

                });
    }
    ;

    function deleteVendorAccountForUser(userId, vendorAccount, callback, auth_token) {
        var userRequestUrl = baseurl + 'customers/' +userId + '/vendor_accounts';
        var vendorObject = {"vendor_id": vendorAccount.vendor_id};
        var vendorObjectAsJson = JSON.stringify(vendorObject);
        $http.post(userRequestUrl, vendorObjectAsJson, {
            headers: {'Authorization': auth_token}})
                .success(function (data) {
                    console.log('Success post rating WS ' + JSON.stringify(data));
                    callback('');
                })
                .error(function (data) {
                    if (data && data.success === "false") {
                        console.log('Error in post rating WS ' + JSON.stringify(data));
                        callback({"error": "Error in request creation"});
                    }

                });
    }
    ;
      
function getVendorsList(callback) {
        var vendorUrl = baseUrl + 'vendors?sort_by=id';
        console.log('Auth_token'+authTokenForSession);
        $http.get(vendorUrl,{
    headers: {'Authorization':authTokenForSession}})
                .success(function (data) {
                    console.log('Success getVendors WS ' + JSON.stringify(data));
                    callback(data);
                })
                .error(function (data) {
                    console.log('Error getVendors WS ' + JSON.stringify(data));
                    if (data.success === "false") {
                        callback({"error": "Error in retrieving vendors"});
                    }

                });


    };

}