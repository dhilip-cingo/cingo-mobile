/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

angular.module('Cingo.services', []).factory('DBService', ['$q', DBService])
function DBService($q) {
    var _db;
    var _globalUser;
    var _sessionSettings;
    return {
        initDB: initDB,
        addUser: addUser,
        getGlobalSettings: getGlobalSettings,
        setGlobalSettings: setGlobalSettings,
        getRequest: getRequest,
        createNewRequest:createNewRequest,
        checkIfPasswordChanged: checkIfPasswordChanged,
        getSecuritySettings:getSecuritySettings,
        saveSessionSettings:saveSessionSettings,
        addVendor:addVendor,
        saveVendorsList:saveVendorsList,
        getVendorsList:getVendorsList,
        saveDocument:saveDocument,
        getDocument:getDocument,
        saveVendor:saveVendor,
        getVendorAccounts:getVendorAccounts,
        saveRequest:saveRequest,
        getRequests:getRequests
    };

    function initDB() {
        // Creates the database or opens if it already exists
        _db = new PouchDB('Cingo', {adapter: 'websql'});
    }
    ;
    function checkIfPasswordChanged(password){
        console.log(" "+_globalUser.password + "==" + JSON.stringify(password))
        if (_globalUser.password === password){
            return true;
        }
        else{
            return false;
        }
    }
    function addUser(user) {
        var userDocument = {
            "_id": "globalSettings",
            "email": user.email,
            "password": user.password,
            "customer_id":user.customer_id
        };

        return $q.when(_db.get('globalSettings').then(function (doc) {
            if (doc !== undefined){
            userDocument._rev = doc._rev;
        }
            _db.put(userDocument);
        }).then(function (response) {
            // handle response
        }).catch(function (err) {
            console.log(err);
        }));

    }
    ;
    
    function addVendor(vendor) {
        console.log('Vendor '+ JSON.stringify(vendor));
        var userDocument = {
            "_id": "vendor/"+vendor.id,
            "email": vendor.email,
            "mobileNumber": vendor.mobile_number,
            "name":vendor.name,
            "logoUrl":vendor.logo.url,
            "companyName":vendor.company_name,
            "docId":"vendors"
        };

        return  $q.when(_db.post(userDocument).then(function (response) {
            // handle response
            console.log("save response for add vendor " + JSON.stringify(response));
        }).catch(function (err) {
            console.log(JSON.stringify(err));
        }));

    }
    ;
    
    function saveSessionSettings(token,authToken,session) {
        _sessionSettings={};
        _sessionSettings.token = token;
        _sessionSettings.auth_token = authToken;
        _sessionSettings.session = session;
        var sessionDocument = {
            "_id": "sessionSettings",
            "token": token,
            "auth_token":authToken,
            "session":session
        };
        return $q.when(_db.put(sessionDocument).then(function (response) {
            // handle response
            console.log("response " + JSON.stringify(response));
        }).catch(function (err) {
            console.log(err);
        }));

    }
    ;
    
    function getSecuritySettings(callback) {
        if (_sessionSettings !== undefined){
            callback(_sessionSettings);
            return;
        }
        if (_db === undefined){
            initDB();
        }
        
        return $q.when(_db.get('sessionSettings', function (err, doc) {
                if (err) {
                    return console.log(err);
                }
                else{
                    if (doc.token !== undefined){
                    _sessionSettings = {};
                    _sessionSettings.token = doc.token;
                    _sessionSettings.auth_token = doc.auth_token;
                    _sessionSettings.session = doc.session;
                    callback(_sessionSettings);
                }
                }
            }));
        };
    function onDatabaseChange(change) {
       
    };


    function getGlobalSettings(callback) {
        if (_db === undefined){
            initDB();
        }
   
        return $q.when(_db.get('globalSettings', function (err, doc) {
                if (err) {
                    return console.log(err);
                }
               console.log("response doc " + JSON.stringify(doc));
                var address;
                var user = {
                    "email":doc.email,
                    "password":doc.password,
                    "firstName":doc.firstName,
                "lastName":doc.lastName,
                "mobile":doc.mobile,
                "customer_id":doc.customer_id,
                _rev:doc._rev
                }
                if (doc.address === undefined){
                   address  =  {
                "street":"",
                "city":"",
                "state":"",
                "zip":""
            } 
                }
                else{
                     address =  {
                "street":doc.address.street,
                "city":doc.address.city,
                "state":doc.address.state,
                "zip":doc.address.zip
            } 
          
                }
                 user.address = address;
                 _globalUser = user
                  console.log("response user " + JSON.stringify(user));
                  if (callback != undefined){
                 callback(user);
                  }
            }));
    }
    ;

    function setGlobalSettings(user) {
        console.log("Global rev "+_globalUser._rev);
        if (user.address === undefined){
            user.address =  {
                "street":"",
                "city":"",
                "state":"",
                "zip":""
            } 
        }
         var userDocument = {
            "_id": "globalSettings",
            "_rev":_globalUser._rev,
            "email": user.email,
            "password": user.password,
            "firstName":user.firstName,
            "lastName":user.lastName,
             "mobile":user.mobile,
            "address":{
            "street":user.address.street,
            "city":user.address.city,
            "state":user.address.state,
            "zip":user.address.zip
        }
        };
        _globalUser = userDocument;
   console.log("save response " + JSON.stringify(_globalUser));        
        return $q.when(_db.put(userDocument).then(function (response) {
            // handle response
            console.log("save response " + JSON.stringify(response));
        }).catch(function (err) {
            console.log(JSON.stringify(err));
        }));

    }
    ;
    
     function createNewRequest(request) {
        console.log("Global rev "+_globalUser._rev);
         var requestDocument = {
            "_id": "request",
            "_rev":"1-2913f4c2b0f76458f4a9eb220c343475",
            "request":request
        }
       
   console.log("save response " + JSON.stringify(_globalUser));        
        return $q.when(_db.post(requestDocument).then(function (response) {
            // handle response
            console.log("save response " + JSON.stringify(response));
        }).catch(function (err) {
            console.log(JSON.stringify(err));
        }));
    }
    ;
    
    function getRequest(callback){
        return $q.when(_db.get('request', function (err, doc) {
                if (err) {
                    if (err.status === 404){
                        return callback(0);
                    }
                    return console.log(JSON.stringify(err));
                }
     
               console.log("response doc " + JSON.stringify(doc));
               
                var request = doc.request;
                request._rev = doc._rev;
                  console.log("response user " + JSON.stringify(request));
                  if (callback != undefined){
                 callback(request);
                  }
            }));
    }
    
    function userObject(){
      var user = {
                    "email":"",
                    "password":"",
                    "firstName":"",
                "lastName":"",
                "address":{
                "street":"",
                "city":"",
                "state":"",
                "zip":""
            }
    }
    return user;
};

function saveVendorsList(vendorsList,callback){
     saveDocument('VendorsList',vendorsList,callback);
};

function getVendorsList(callback){
        getDocument('VendorsList',callback);
}

function getDocument(documentName,callback){
 _db.get(documentName, function(err, doc) {
  if (err) { 
      console.log('db save object response'+JSON.stringify(err));
      callback(err);
  }
  console.log('db save object response'+JSON.stringify(response));
  
  callback(doc.object);
  // handle doc
});
    
};

    function saveDocument(propertyName,objectToBeSaved,callback){
      _db.get(propertyName, function(err, doc) {
          var abstractedObject={};
             if (doc !== undefined){
            
             abstractedObject = {
                 "_id":propertyName,
                 "_rev":doc.rev,
                 "object":objectToBeSaved
             };
             
             console.log('db saving '+ JSON.stringify(objectToBeSaved));            
        }
        else{
             console.log('db saving as new '+ JSON.stringify(objectToBeSaved));
              abstractedObject = {
                 "_id":propertyName,
                 "object":objectToBeSaved
             };
   
           
        }
        _db.put(abstractedObject,function(err, response) {
    if (err) {  
        console.log('db save object response'+JSON.stringify(err));
        callback();
   }
   else{
     console.log('db save object response'+JSON.stringify(response));
      callback();
 }
           
  });
});  
     };
 function saveVendor(vendorObject,vendorSettings,callback){
     delete vendorSettings._rev;
    var propertyName = 'UserVendorAccountss';
    vendorObject.settings = vendorSettings;
      _db.get(propertyName, function(err, doc) {
          var abstractedObject={};
             if (doc !== undefined){
            
            var vendors = doc.vendors;
            vendors.push(vendorObject);
             abstractedObject = {
                 "_id":propertyName,
                 "_rev":doc._rev,
                 "vendors":vendors
             };
             
             console.log('db saving '+ JSON.stringify(abstractedObject));            
        }
        else{
             
             var vendors = [];
             vendors.push(vendorObject);
              abstractedObject = {
                 "_id":propertyName,
                 "vendors":vendors
             };
   console.log('db saving as new '+ JSON.stringify(abstractedObject));
           
        }
        _db.put(abstractedObject,function(err, response) {
    if (err) {  
        console.log('db save object response'+JSON.stringify(err));
        callback();
   }
   else{
     console.log('db save object response'+JSON.stringify(response));
      callback();
 }
           
  });
});  
     }; 
     
     function getVendorAccounts(callback){
    var propertyName = 'UserVendorAccountss';
      _db.get(propertyName, function(err, doc) {
     
             if (doc !== undefined){
            
            var vendors = doc.vendors;
             console.log('db fetched '+ JSON.stringify(doc));  
            callback(vendors);
                      
        }
        else{
             
             var vendors = [];
              callback(vendors);
  
           
        }
});  
     }; 
     
      function saveRequest(requestObject,callback){
    var propertyName = 'requests';
      _db.get(propertyName, function(err, doc) {
          var abstractedObject={};
             if (doc !== undefined){
            var requests = doc.requests;
            requests.push(requestObject);
             abstractedObject = {
                 "_id":propertyName,
                 "_rev":doc._rev,
                 "requests":requests
             };
             
             console.log('db saving '+ JSON.stringify(abstractedObject));            
        }
        else{
             
             var requests = [];
             requests.push(requestObject);
              abstractedObject = {
                 "_id":propertyName,
                 "requests":requests
             };
   console.log('db saving as new '+ JSON.stringify(abstractedObject));
           
        }
        _db.put(abstractedObject,function(err, response) {
    if (err) {  
        console.log('db save object response'+JSON.stringify(err));
        callback();
   }
   else{
     console.log('db save object response'+JSON.stringify(response));
      callback();
 }
           
  });
});  
     }; 
     
     function getRequests(callback){
    var propertyName = 'requests';
      _db.get(propertyName, function(err, doc) {
             if (doc !== undefined){
            var requests = doc.requests;
             console.log('db fetched '+ JSON.stringify(doc));  
            callback(requests);              
        }
        else{  
             var requests = [];
              callback(requests); 
        }
});  
     };
}